const {
    app,
    shell,
    BrowserWindow,
    globalShortcut
} = require('electron');

const fs = require('fs')
const path = require('path')

const Config = require('./config')

const protocol = 'https'
const domain = 'postfix.hci.uni-hannover.de'

let config
let windows = []

function injectCss(win) {
    let css_path = path.join(__dirname, 'inject.css')
    let css = fs.readFileSync(css_path).toString()
    win.webContents.insertCSS(css)
}

function handleRedirect(e, url) {
    if (!url.startsWith(protocol + '://' + domain)) {
        e.preventDefault()
        shell.openExternal(url)
    }
}


function createWindow() {
    let options = {
        width: config.window ? config.window.width || 800 : 800,
        height: config.window ? config.window.height || 600 : 600,
        autoHideMenuBar: true,
        titleBarStyle: 'hiddenInset',
        webPreferences: {
            nodeIntegration: false,
            webSecurity: true
        }
    }

    let win = new BrowserWindow(options)

    if (config.window && config.window.fullscreen) {
        win.maximize()
    }

    win.on('close', () => {
        config = {
            window: Object.assign(
                win.getBounds(),
                { fullscreen: win.isMaximized() }
            )
        }

        Config.save(config)
    })

    win.on('closed', () => {
        let i = windows.indexOf(win)
        if (i >= 0) {
            windows.splice(i, 1)
        } else {
            console.log("Error: Window could not be closed!")
        }
    })

    win.webContents.on('did-finish-load', () => {
        injectCss(win)
    });

    win.webContents.on('will-navigate', handleRedirect)
    win.webContents.on('new-window', handleRedirect)

    win.loadURL(protocol + '://' + domain)

    windows.push(win)
}

app.on('ready', () => {
    config = Config.load()

    globalShortcut.register('CommandOrControl+N', createWindow)

    createWindow()
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (windows.length === 0) {
        createWindow()
    }
})
